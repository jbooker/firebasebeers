
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewBeersComponent } from './view-beers.component';

describe('ViewBeersComponent', () => {
  let component: ViewBeersComponent;
  let fixture: ComponentFixture<ViewBeersComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewBeersComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewBeersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
