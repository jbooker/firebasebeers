import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { ViewBeersDataSource, ViewBeersItem } from './view-beers-datasource';
import { AngularFireDatabase } from 'angularfire2/database';
import { pipe, Subscription } from 'rxjs';
import { map, first } from 'rxjs/operators';

@Component({
  selector: 'app-view-beers',
  templateUrl: './view-beers.component.html',
  styleUrls: ['./view-beers.component.css']
})
export class ViewBeersComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: ViewBeersDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['date', 'text'];
  subscription: Subscription;

  constructor(private db: AngularFireDatabase) {
  }

  ngOnInit() {
    this.subscription = this.db.list<ViewBeersItem>('beers').valueChanges().pipe(first()).subscribe(d => {
      console.log('data streaming');
      this.dataSource = new ViewBeersDataSource(this.paginator, this.sort);
      this.dataSource.data = d;
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
