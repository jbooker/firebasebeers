import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'app-add-beer',
  templateUrl: './add-beer.component.html',
  styleUrls: ['./add-beer.component.css']
})
export class AddBeerComponent implements OnInit {

  beer: any = {};

  constructor(private db: AngularFireDatabase) { }

  ngOnInit() {}

  onSubmit() {
    this.beer.date = new Date(this.beer.date).valueOf();
    this.db.list('beers').push(this.beer)
      .then(_ => {
        this.beer = {};
        console.log('success');
      });
  }

}
